CLUSTER_NAME=${CLUSTER_NAME:-k3d-default}
RELEASE_NAME=gitlab
EXTERNAL_IP=${EXTERNAL_IP:-$(host -4 $(hostname) | awk '{print $NF}' | head -n1)}
GL_HOME=${GL_HOME:-$(pwd)}
CERT_MANAGER_EMAIL=${CERT_MANAGER_EMAIL:-$(git config user.email)}