#!/bin/sh

source gl_deployment_config.sh

kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo
