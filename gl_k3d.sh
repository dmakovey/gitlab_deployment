#!/bin/sh

source gl_deployment_config.sh

CLUSTER_NAME=${1:-${CLUSTER_NAME}}

# Follow the pattern that Kind configs are following:
k3d cluster create ${CLUSTER_NAME} -v /dev/mapper:/dev/mapper -p "443:32443@loadbalancer" -p "32022:32022@loadbalancer" -p "80:32080@loadbalancer"