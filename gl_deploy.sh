#!/bin/sh

source gl_deployment_config.sh

cd ${GL_HOME}

helm upgrade --install gitlab . \
  --set global.hosts.domain=${EXTERNAL_IP}.nip.io \
  --set global.hosts.externalIP=${EXTERNAL_IP}
  --set certmanager-issuer.email=${CERT_MANAGER_EMAIL} \
  -f examples/kind/values-base.yaml \
  -f examples/kind/values-ssl.yaml
