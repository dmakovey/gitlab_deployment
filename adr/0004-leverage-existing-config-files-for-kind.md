# 4. Leverage existing config files for Kind

Date: 2021-05-14

## Status

Accepted

## Context

There are no `k3s`-specific configuration files for faster/easier deployment.

## Decision

We can (and should) leverage existing configs to lower maintenance burden and bring dev experience to a common denominator.

## Consequences

we are using `examples/kind/values-*.yaml` files for Helm deployment
