# 3. Use git credentials by default

Date: 2021-05-14

## Status

Accepted

## Context

Cert-manager requires an email address. 
We also need to make scripts useful by default.

## Decision

use Git's configured email:

```shell
git config user.email
```

since more likely than not it is valid and belongs to person executing script

Leave option to override this default behavior in place

## Consequences

Developer doesn't have to specify email and default behavior will be reasonable. However should developer
decide to provide alternate email he still can by setting up env variable:

```shell
export CERT_MANAGER_EMAIL=my-other@email.com
```
