# Simplified Gitlab deployment scripts

Set of scripts to stand up local instance of GitLab in `k3s` cluster managed by `k3d`

## Sample flow

We need clone of `gitlab-org/charts/gitlab` repo first:

```shell
cd ${HOME}/some/dir
git clone git@gitlab.com:gitlab-org/charts/gitlab.git
GL_HOME=${HOME}/some/dir/gitlab
```
Now we need `gitlab_deployment` clone:

```shell
git clone git@gitlab.com:dmakovey/gitlab_deployment.git
cd gitlab_deployment
```

from there we need to:

1. setup cluster
2. deploy GL helm chart
3. (optional) if there are issues reaching `nip.io` - add entries to `/etc/hosts`
4. get root password of fresh install

```shell
# create cluster (following command will also switch context to a newly created cluster)
./gl_k3d.sh my-cluster
# at this point your default context will be `my-cluster`

# deploy helm chart
GL_HOME=${HOME}/some/dir/gitlab ./gl_deploy.sh

# if NIP.IO is blocked we need to substitute:
./gl_etc_hosts.sh >> /etc/hosts

# grab root password to be able to login:
./gl_get_root_password.sh
```